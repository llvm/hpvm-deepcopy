#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "heterocc.h"

// User code

void stencil(char* buf, size_t buf_size) {
  void* Section = __hetero_section_begin();
  void* Wrapper = __hetero_task_begin(1, buf, buf_size, 1, buf, buf_size);
  __hpvm__hint(hpvm::GPU_TARGET);

  int i = 0;
  i += 1;

  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}

int main(int argc, char* argv[]) {
  char* buf = (char*)calloc(100, 1);
  void* DFG = __hetero_launch((void*)stencil, 1, buf, 100, 1, buf, 100);
  __hetero_wait(DFG);

  std::cout << "After kernel in buffer:\n";

  return 0;
}
