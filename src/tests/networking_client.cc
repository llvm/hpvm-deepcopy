#define USE_HPVM
#ifdef USE_HPVM
#include "heterocc.h"
#endif
#include "hpvm_dclib.hpp"
// Make sure dclib is at front of everything

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "base64.hpp"

using namespace std;
// A linked list:
class LNode {
 public:
  size_t val;
  int* other_vals = nullptr;  // arbitrary long array

  LNode* next = nullptr;

  inline LNode(int v) {
    val = v;
    other_vals = hpvm_new_arr<int>(5);
    for (int i = 0; i < 5; i++) other_vals[i] = i + 1 + v;
  }

  static LNode* create_nodes(initializer_list<int> vs) {
    LNode *root = nullptr, *curr = nullptr;
    for (auto v : vs) {
      if (!root)
        root = curr = hpvm_new<LNode>(v);
      else
        curr = curr->next = hpvm_new<LNode>(v);
    }
    return root;
  }

  void delta(int x) {
    for (int i = 0; i < 5; i++) other_vals[i] += x;
  }

  virtual void print() {
    cout << "[Node " << val << ": ";
    for (int i = 0; i < 5; i++) cout << other_vals[i] << ",";
    cout << "]\n -> ";

    if (next)
      next->print();
    else
      cout << "null"
           << "\n";
  }
};

void dummy(void* _, size_t __) {
  void* Section = __hetero_section_begin();
  void* Wrapper = __hetero_task_begin(1, _, __, 1, _, __);
  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}

int main(int argc, char* argv[]) {
  cout << "Verification input decode to the same buffer: ";
  string inp;
  cin >> inp;
  size_t decoded_len = 0;
  char* decoded =
      (char*)base64_decode((const char*)inp.c_str(), inp.size(), decoded_len);

  // Verification

  cout << "Get node:\n";
  HpvmBuf client_buf;
  client_buf.buf = (HpvmBufHeader*)decoded;
  auto* client_node = remap_for_use<LNode>(client_buf);
  client_node->print();

  // make hpvm compiler happy
  void* DFG = __hetero_launch((void*)dummy, 1, client_node, 0, 1, client_node, 0);
  __hetero_wait(DFG);
  return 0;
}
