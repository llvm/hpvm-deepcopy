#define USE_HPVM
#ifdef USE_HPVM
#include "heterocc.h"
#endif
#include "hpvm_dclib.hpp"
// Make sure dclib is at front of everything

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

// User code

// A linked list:
class LNode {
 public:
  size_t val;

  LNode* next = nullptr;

  inline LNode(int v) { val = v; }
  void delta(int x) { val += x; }
  void print() {
    auto curr = this;
    do {
      std::cout << "[Node " << (void*)curr << " | " << curr->val << "] -> ";
      curr = curr->next;
    } while (curr != this);
    std::cout << "[loop back]\n";
  }
  static LNode* create_nodes(std::initializer_list<int> vs) {
    LNode *root = nullptr, *curr = nullptr;
    for (auto v : vs) {
      if (!root)
        root = curr = hpvm_new<LNode>(v);
      else
        curr = curr->next = hpvm_new<LNode>(v);
    }
    curr->next = root;
    return root;
  }
};

#ifdef USE_HPVM
void ring_buf_kernel(HpvmBufHeader* buf_header, size_t header_size) {
  void* Section = __hetero_section_begin();

  void* Wrapper = __hetero_task_begin(1, buf_header, header_size, 1, buf_header,
                                      header_size);
  __hpvm__hint(hpvm::GPU_TARGET);
  auto* root = remap_for_use<LNode>(buf_header);  // remap for use

  auto* curr = root;
  for (int i = 0; i < 8; i++) {
    // pointer comparison seems unsupported on device:
    // see `llvm/tools/hpvm/lib/CoreHPVM/DFG2LLVM.cpp:712`
    curr->delta(1);  // increment all values in node by 1
    curr = curr->next;
  }

  remap_for_copy(buf_header);  // remap for copy
  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}
#endif

using namespace std;
int main(int argc, char* argv[]) {
  auto* root = LNode::create_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  cout << "Before kernel:\n";
  root->print();

  cout << "Kernel launch!\n";
  auto host_buf = hpvm_do_snapshot(root);
  void* DFG = __hetero_launch((void*)ring_buf_kernel, 1, host_buf.buf,
                              host_buf.buf->total_size(), 1, host_buf.buf,
                              host_buf.buf->total_size());
  __hetero_wait(DFG);

  root = remap_for_use<LNode>(host_buf);
  cout << "After kernel in buffer:\n";
  root->print();

  return 0;
}
