#define USE_HPVM
#ifdef USE_HPVM
#include "heterocc.h"
#endif
#include "hpvm_dclib.hpp"
// Make sure dclib is at front of everything

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

// User code

// A linked list:
class LNode {
 public:
  size_t val;
  int* other_vals = nullptr;  // arbitrary long array

  LNode* next = nullptr;

  inline LNode(int v) {
    val = v;
    other_vals = hpvm_new_arr<int>(5);
    for (int i = 0; i < 5; i++) other_vals[i] = i + 1 + v;
  }
  void delta(int x) {
    val += x;
    for (int i = 0; i < 5; i++) other_vals[i] += x;
  }
  void print() {
    std::cout << "[Node " << val << ": ";
    for (int i = 0; i < 5; i++) std::cout << other_vals[i] << ",";
    std::cout << "] -> ";

    if (next)
      next->print();
    else
      std::cout << "null"
                << "\n";
  }
  static LNode* create_nodes(std::initializer_list<int> vs) {
    LNode *root = nullptr, *curr = nullptr;
    for (auto v : vs) {
      if (!root)
        root = curr = hpvm_new<LNode>(v);
      else
        curr = curr->next = hpvm_new<LNode>(v);
    }
    return root;
  }
};
// A structure behave the same as LNode, but without the user-provided copy
// function
struct NonUserCopyStruct : LNode {
 public:
  LNode* node = nullptr;
  using LNode::LNode;

  inline static void hpvm_snapshot_custom_behavior(HpvmBuf& buf, LNode* dst,
                                                   LNode* src) {
    snapshot_pointer(buf, dst->other_vals, src->other_vals);
    snapshot_pointer(buf, dst->next, src->next);
  }
};

#ifdef USE_HPVM
void stencil(HpvmBufHeader* buf_header, size_t header_size) {
  void* Section = __hetero_section_begin();

  void* Wrapper = __hetero_task_begin(1, buf_header, header_size, 1, buf_header,
                                      header_size);
  __hpvm__hint(hpvm::GPU_TARGET);

  auto* root = use_on_device<LNode>(buf_header);

  root->next->val = 100;
  while (root) {
    root->delta(1);
    root = root->next;
  }

  done_use_on_device(buf_header);

  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}
#endif

int main(int argc, char* argv[]) {
  LNode* host_obj = LNode::create_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  std::cout << "Before kernel:\n";
  host_obj->print();
  std::cout << "Has value:" << has_custom_behavior<LNode>::value << "\n";
  static_assert(has_custom_behavior<NonUserCopyStruct>::value, "");

  // auto _ = hpvm_do_snapshot(new NonUserCopyStruct{1});
  auto host_buf = hpvm_do_snapshot(host_obj);

#ifdef USE_HPVM
  std::cout << "Kernel launch!" << std::endl;
  void* DFG = __hetero_launch((void*)stencil, 1, host_buf.buf,
                              host_buf.buf->total_size(), 1, host_buf.buf,
                              host_buf.buf->total_size());
  __hetero_wait(DFG);
#else
  auto buf_header = host_buf.buf;
  buf_header->to_absolute_pointer(buf_header->get_obj_start());
  auto* root = buf_header->get_obj_start<LNode>();
  root->next->val = 100;
  while (root) {
    root->delta(1);
    root = root->next;
  }

  buf_header->to_relative_pointer(buf_header->get_obj_start());
#endif

  std::cout << "After kernel in buffer:\n";
  host_buf.recover_host_accessible_obj<LNode>()->print();

  return 0;
}
// User code end---------------------
// User / HPVM Codegen provides:
// template <>
// void hpvm_snapshot_custom<LNode>(HpvmBuf& buf, LNode* dst_obj,
// LNode* original_obj) {
//// recursive allocation & copies
//// snapshot_pointer(buf, dst_obj->other_vals, original_obj->other_vals);
//// snapshot_pointer(buf, dst_obj->next, original_obj->next);
// dispatch_snapshot(buf, dst_obj->other_vals, original_obj->other_vals);
// dispatch_snapshot(buf, dst_obj->next, original_obj->next);
//}
// User / HPVM Codegen end ------------
