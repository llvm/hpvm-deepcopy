#define USE_HPVM
#ifdef USE_HPVM
#include "heterocc.h"
#endif
#include "hpvm_dclib.hpp"
// Make sure dclib is at front of everything

#define EIGEN_NO_DEBUG  // necessary to prevent unreachable in eigen
// necessary to prevent exception in itlib
#define ITLIB_SMALL_VECTOR_ERROR_HANDLING ITLIB_SMALL_VECTOR_ERROR_HANDLING_NONE
#define ITLIB_SMALL_VECTOR_NO_DEBUG_BOUNDS_CHECK

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <functional>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "./eigen/Eigen/Dense"
#include "./itlib/include/itlib/small_vector.hpp"

// User code

using namespace Eigen;
using ContainerType = std::vector<int, DeepCopyAlloc<int>>;

// itlib vector
// using ContainerType = itlib::small_vector<int, 0, 0, DeepCopyAlloc<int>>;

// Boost vector
// using namespace boost::numeric::ublas;
// using ContainerType = vector<int>;

#ifdef USE_HPVM
// clang-format off
void stencil(HpvmBufHeader* vecbh, size_t vecbh_size) {
  // clang-format on
  void* Section = __hetero_section_begin();

  // clang-format off
  void* Wrapper = __hetero_task_begin(1, 
                                      vecbh, vecbh_size, 
                                      1, vecbh, vecbh_size);
  // clang-format on
  __hpvm__hint(hpvm::GPU_TARGET);

  auto& vec_device = *use_on_device<ContainerType>(vecbh);

  //((int*)vec_device.data())[0] = 0;
  vec_device[0] = 100;

  done_use_on_device(vecbh);

  __hetero_task_end(Wrapper);
  __hetero_section_end(Section);
}
#endif

int main(int argc, char* argv[]) {
  // ContainerType vec = {1, 2, 3, 4, 5, 6};
  ContainerType vec(8);
  for (int i = 0; i < 8; i++) vec[i] = i + 1;

  for (int i = 0; i < 8; i++) std::cout << vec[i] << ",";
  std::cout << ";\n";

  auto vecb = hpvm_do_snapshot(&vec);

#ifdef USE_HPVM
  std::cout << "Kernel launch!" << std::endl;
  // clang-format off
  void* DFG =
      __hetero_launch((void*)stencil, 1, 
                      vecb.buf, vecb.buf->total_size(),
                      1, vecb.buf, vecb.buf->total_size());
  // clang-format on
  __hetero_wait(DFG);
#else
  auto& vec_device = *use_on_device<ContainerType>(vecb.buf);

  vec[0] = 100;

  done_use_on_device(vecb.buf);
#endif

  std::cout << "After kernel in buffer:\n";
  auto& recovered_vec = *vecb.recover_host_accessible_obj<ContainerType>();
  for (int i = 0; i < 8; i++) std::cout << recovered_vec[i] << ",";
  std::cout << ";\n";

  return 0;
}
